package marketstore;

public class SilverCard extends Card {

	public SilverCard(double turnover)
	{
		this.turnover=turnover;
		
		if(this.turnover<=300)
			this.discountRate=0.02;//2%
		else
			this.discountRate=0.035;//3.5%
	}

}
