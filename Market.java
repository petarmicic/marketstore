package marketstore;

public class Market {

	public static void main(String[] args) 
	{
		
		Card bronze=new BronzeCard(0);
		Card silver=new SilverCard(600);
		Card gold=new GoldCard(1500);
		
		bronze.purchase(150);
		silver.purchase(850);
		gold.purchase(1300);

	}

}
