package marketstore;

public class GoldCard extends Card {

	public GoldCard(double turnover)
	{
		this.turnover=turnover;
		
		this.discountRate=0.02+this.turnover/10000;
		
		if(this.discountRate>0.1)
			this.discountRate=0.1;//10%
	}

}
