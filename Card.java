package marketstore;

public abstract class Card {
	protected double turnover;
	protected double discountRate;
	
	public double getDiscountRate()
	{
		return this.discountRate;
	}
	
	public double getTurnover()
	{
		return this.turnover;
	}
	
	public double[] purchase(double purchaseValue) 
	{	
		double discount= purchaseValue*this.discountRate;
		double result=purchaseValue-discount;
		
		double[] arrayToReturn= {this.discountRate, discount, result};
		
		System.out.printf("Purchase value: $%.2f%n",purchaseValue);
		System.out.printf("Discount rate: %.1f%%%n",this.discountRate*100);
		System.out.printf("Discount: $%.2f%n",discount);
		System.out.printf("Total purchase value: $%.2f%n", result);
		
		return arrayToReturn;
	}
	
}
