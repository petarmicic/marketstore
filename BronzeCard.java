package marketstore;

public class BronzeCard extends Card {

	public BronzeCard(double turnover) 
	{
		this.turnover=turnover;
		
		if(this.turnover<=100)
			this.discountRate=0;
		else if(this.turnover>100 && this.turnover<=300) 
			this.discountRate=0.01;//1%
		else
			this.discountRate=0.025;//2.5%
		
	}
	

}
